# Demo 21.11.2023

## Pokretanje

- Kreirajte virtualni env

```
python -m venv env
```

- Aktivirajte virtualni env

```
source env/Scripts/activate
```

- instalirajte django

```
pip install django
```

- pokrenite migracije i server

```
cd myimgur
python manage.py migrate
python manage.py runserver
```
