from django.contrib import admin
from django.urls import path

from images.views import homepage
from images.views import test
from images.views import image_detail
from images.views import image_create

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', homepage, name='homepage'),
    path('test/', test, name='test'),
    path('slike/<int:image_id>/', image_detail, name='image_detail'),
    path('slike/new/', image_create, name='image_create'),
]
