from django.db import models

# Create your models here.
class Image(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    url = models.CharField(max_length=512)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.title

class Comment(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    text = models.TextField()
    author = models.CharField(max_length=200)
