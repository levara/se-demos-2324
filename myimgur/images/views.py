from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse


from .models import Image

def homepage(request):
    images = Image.objects.all()
    context = {
        'slike': images
    }
    return render(request, 'images/homepage.html', context)

def test(request):
    return HttpResponse("Test")

def image_detail(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    context = {
        'image': image,
    }

    return render(request, 'images/detail.html', context)

def image_create(request):
    if request.method == 'POST':
        title = request.POST.get('title', '')
        description = request.POST.get('description', '')
        url = request.POST.get('url', '')

        image = Image(title=title, description=description, url=url)
        image.save()

        return HttpResponseRedirect(reverse('image_detail', args=(image.id,)))

    else:
        context = {}
        return render(request, 'images/create.html', context)

# Create your views here.
