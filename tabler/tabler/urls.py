from django.contrib import admin
from django.urls import path
from django.urls import include


from app.views import homepage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', homepage, name='homepage'),
    path('stolovi/', include('app.urls')),
]
