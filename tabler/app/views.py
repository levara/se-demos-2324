from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from app.models import Table
from app.models import Article
from app.models import Order
from app.models import OrderItem

def homepage(request):
    context = {}
    return render(request, 'app/homepage.html', context)

def tables_list(request):
    free_tables = Table.objects.filter(occupied=False).order_by('-number')
    occupied_tables = Table.objects.filter(occupied=True).order_by('-number')
    context = { 
        'free_tables': free_tables,
        'occupied_tables': occupied_tables,
    }
    return render(request, 'app/table_list.html', context)

def table_detail(request):
    code = request.GET.get('code')
    table = Table.objects.get(code=code)

    articles = Article.objects.all().order_by('price')

    items = OrderItem.objects.filter(order__table=table)

    context = {
        'stol': table,
        'articles': articles,
        'items': items,
    }
    return render(request, 'app/table_detail.html', context)

def create_order(request, table_id):
    table = Table.objects.get(id=table_id)
    table.occupied = True
    table.save()

    order = Order.objects.create(table=table)

    article_ids = request.POST.getlist('article')
    articles = Article.objects.filter(id__in=article_ids)
    items = []
    for article in articles:
        item = OrderItem.objects.create(
            order=order,
            article=article,
            quantity=1,
        )

    return HttpResponseRedirect('/table/?code=' + table.code)
