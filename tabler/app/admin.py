from django.contrib import admin
from .models import Table
from .models import Article

# Register your models here.
admin.site.register(Table)
admin.site.register(Article)
