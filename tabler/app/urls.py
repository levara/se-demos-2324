from django.urls import path
from .views import tables_list
from .views import create_order
from .views import table_detail


urlpatterns = [
    path('', tables_list, name='tables_list'),
    path('<int:table_id>/order/', create_order, name='create_order'),
    path('table/', table_detail, name='table_detail')
]
