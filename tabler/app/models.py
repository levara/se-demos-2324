from django.db import models

# Create your models here.
class Table(models.Model):
    number = models.IntegerField()
    capacity = models.IntegerField()
    occupied = models.BooleanField(default=True)
    code = models.CharField(max_length=10)

    def __str__(self):
        return f'Table {self.number} [{self.code}]'

class Article(models.Model):
    price = models.DecimalField(max_digits=8, decimal_places=2)
    name = models.CharField(max_length=100)
    size = models.CharField(max_length=20)

    def __str__(self):
        return f'{self.name} ({self.size}) - {self.price} EUR'


class Order(models.Model):
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    discount = models.DecimalField(max_digits=5, decimal_places=2, default=0)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    quantity = models.IntegerField()

