from django.contrib import admin
from django.urls import path
from app import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # mojapicerija.hr/pero/
    # mojapicerija.hr/
    path('', views.homepage, name='homepage'),
    path('venues/', views.venues_list, name='venues_list'),
    path('venues/<int:venue_id>/', views.venue_details, name='venue_details'),
    path('venues/<int:venue_id>/order_confirmation/', views.order_confirmation, name='order_confirmation'),
    path('venues/<int:venue_id>/order/', views.create_order, name='create_order'),
]
