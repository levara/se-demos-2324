from django.shortcuts import render
from django.http import HttpResponse

from .models import Venue, Pizza, Order, OrderItem

# Create your views here.
def homepage(request):
    return render(request, 'app/homepage.html')

def venues_list(request):
    venues = Venue.objects.order_by('name')

    context = {
        'venues': venues,
    }
    return render(request, 'app/venues_list.html', context=context)

def venue_details(request, venue_id):
    venue = Venue.objects.get(id=venue_id)
    # pizzas = Pizza.objects.filter(venue=venue)
    pizzas = venue.pizza_set.all()

    context = {
        'venue': venue,
        'pizzas': pizzas,
    }

    return render(request, 'app/venues_detail.html', context=context)

def order_confirmation(request, venue_id):
    venue = Venue.objects.get(id=venue_id)
    pizza_ids = request.POST.getlist('pizza')
    pizzas = Pizza.objects.filter(id__in=pizza_ids)

    order = Order(venue=venue, name='Pero') 
    order.save()

    items = []
    for pizza in pizzas: 
        item = OrderItem(pizza=pizza, order=order, quantity=1)
        item.save()
        items.append(item)

    context = {
        'venue': venue,
        'pizzas': pizzas,
        'order': order,
    }
    return render(request, 'app/order_confirmation.html', context=context)

def create_order(request, venue_id):
    venue = Venue.objects.get(id=venue_id)
    pass
    


