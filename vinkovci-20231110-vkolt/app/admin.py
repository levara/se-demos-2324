from django.contrib import admin
from app.models import Venue, Pizza

# Register your models here.
admin.site.register(Venue)
admin.site.register(Pizza)

