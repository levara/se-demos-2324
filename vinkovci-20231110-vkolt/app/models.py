from django.db import models

# Create your models here.

class Venue(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=20, blank=True)
    working_hours = models.CharField(max_length=100, blank=True)
    image = models.CharField(max_length=512, blank=True)

    def __str__(self):
        return f"{self.id} - {self.name}"

class Pizza(models.Model):
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    size = models.CharField(max_length=20)
    description = models.CharField(max_length=512, blank=True)

    def __str__(self):
        return f"{self.id} - {self.name} ({self.venue.name})"

class Order(models.Model):
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    confirmed = models.BooleanField(default=False)

    def total_price(self):
        items = OrderItem.objects.filter(order=self)
        total = 0
        for item in items:
            total += item.pizza.price * item.quantity
        return total

class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
